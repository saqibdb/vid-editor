//
//  StoryboardSegueComplition.m
//  Videos Sequencer
//
//  Created by iBuildX on 04/04/2018.
//  Copyright © 2018 Saqibdb. All rights reserved.
//

#import "StoryboardSegueComplition.h"

@implementation StoryboardSegueComplition

- (void)perform {
    [super perform];
    if (self.completion != nil) {
        [self.destinationViewController.transitionCoordinator
         animateAlongsideTransition:nil
         completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
             if (![context isCancelled]) {
                 self.completion();
             }
         }];
    }
}

@end
