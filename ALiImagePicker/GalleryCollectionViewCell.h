//
//  GalleryCollectionViewCell.h
//  VidEditor
//
//  Created by iBuildx_Mac_Mini on 10/19/16.
//  Copyright © 2016 LeeWong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GalleryCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *galleryImageView;
@end
