//
//  CropTypeVC.m
//  Videos Sequencer
//
//  Created by iBuildx-Mac3 on 4/10/17.
//  Copyright © 2017 Saqibdb. All rights reserved.
//

#import "CropTypeVC.h"

@interface CropTypeVC ()

@end

@implementation CropTypeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view layoutIfNeeded];
    for (UIButton *btn in self.colorPlates) {
        btn.layer.cornerRadius =6;
        
    }
    self.portraitView.backgroundColor = [UIColor colorWithRed:225/255.0 green:226/255.0 blue:231/255.0 alpha:1];
    self.landscaperView.backgroundColor = [UIColor whiteColor];
    self.selectedColorView.layer.cornerRadius = 6;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)unwindToCropTypeSelection:(UIStoryboardSegue*)sender
{
}
- (IBAction)selectCropTypeAction:(UIButton *)sender {
    NSString *SelectedOrientation ;
    switch (sender.tag) {
        case 0:
        {
            self.portraitView.backgroundColor = [UIColor colorWithRed:225/255.0 green:226/255.0 blue:231/255.0 alpha:1];
             self.landscaperView.backgroundColor = [UIColor whiteColor];
            
            SelectedOrientation = @"portrait";
            break;
        }
        case 1:
        {
            self.landscaperView.backgroundColor = [UIColor colorWithRed:225/255.0 green:226/255.0 blue:231/255.0 alpha:1];
            self.portraitView.backgroundColor = [UIColor whiteColor];
             SelectedOrientation = @"landscape";
            break;
        }
        default:
            break;
    }
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:SelectedOrientation forKey:@"Orientation"];
    
}

- (IBAction)colorSelectionAction:(UIButton *)sender {
    UIColor *selectedColor;
    
    switch (sender.tag) {
        case 0:
            selectedColor = [UIColor colorWithRed:255/255.0 green:0/255.0 blue:0/255.0 alpha:1];
            break;
        case 1:
            selectedColor = [UIColor colorWithRed:255/255.0 green:204/255.0 blue:102/255.0 alpha:1];
            break;
        case 2:
            selectedColor = [UIColor colorWithRed:102/255.0 green:255/255.0 blue:102/255.0 alpha:1];
            break;
        case 3:
            selectedColor = [UIColor colorWithRed:102/255.0 green:204/255.0 blue:255/255.0 alpha:1];
            break;
        case 4:
            selectedColor = [UIColor colorWithRed:102/255.0 green:102/255.0 blue:255/255.0 alpha:1];
            break;
        case 5:
            selectedColor = [UIColor colorWithRed:204/255.0 green:102/255.0 blue:255/255.0 alpha:1];
            break;
        case 6:
            selectedColor = [UIColor colorWithRed:128/255.0 green:0/255.0 blue:128/255.0 alpha:1];
            break;
        case 7:
            selectedColor = [UIColor colorWithRed:255/255.0 green:128/255.0 blue:0/255.0 alpha:1];
            break;
        case 8:
            selectedColor = [UIColor colorWithRed:0/255.0 green:255/255.0 blue:128/255.0 alpha:1];
            break;
        case 9:
            selectedColor = [UIColor colorWithRed:0/255.0 green:255/255.0 blue:255/255.0 alpha:1];
            break;
        case 10:
            selectedColor = [UIColor colorWithRed:76/255.0 green:76/255.0 blue:76/255.0 alpha:1];
            break;
        case 11:
            selectedColor = [UIColor colorWithRed:128/255.0 green:0/255.0 blue:255/255.0 alpha:1];
            break;
        case 12:
            selectedColor = [UIColor colorWithRed:255/255.0 green:128/255.0 blue:0/255.0 alpha:1];
            break;
        case 13:
            selectedColor = [UIColor colorWithRed:128/255.0 green:128/255.0 blue:0/255.0 alpha:1];
            break;
        case 14:
            selectedColor = [UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1];
            break;
        case 15:
            selectedColor = [UIColor colorWithRed:128/255.0 green:0/255.0 blue:64/255.0 alpha:1];
            break;
            
        default:
            break;
    }
    self.selectedColorView.backgroundColor = selectedColor;
    NSData *colorData = [NSKeyedArchiver archivedDataWithRootObject: selectedColor];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:colorData forKey:@"bgColor"];
}
- (IBAction)nextAction:(UIButton *)sender {
    [self performSegueWithIdentifier:@"cropTypeToJoinVideos" sender:self];
}
@end
