//
//  StoryboardSegueComplition.h
//  Videos Sequencer
//
//  Created by iBuildX on 04/04/2018.
//  Copyright © 2018 Saqibdb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoryboardSegueComplition : UIStoryboardSegue

@property (nonatomic, copy) void(^completion)();


@end
