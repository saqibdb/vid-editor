//
//  PlayerViewController.h
//  VidEditor
//
//  Created by iBuildx_Mac_Mini on 10/19/16.
//  Copyright © 2016 LeeWong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ALiAsset.h"
#import "SAVideoRangeSlider.h"
#import <AVKit/AVKit.h>
#include "RETrimControl.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
@interface PlayerViewController : UIViewController < SAVideoRangeSliderDelegate, AVPlayerViewControllerDelegate,RETrimControlDelegate,AVAudioPlayerDelegate,UIScrollViewDelegate>
    @property (strong, nonatomic) AVAudioPlayer *player;
 @property (nonatomic, strong) NSURL *selectedAudioFileUrl;
@property (nonatomic, strong) NSURL *selectedURL;
@property (weak, nonatomic) IBOutlet UIButton *playBtn;

@property (weak, nonatomic) IBOutlet UIView *playerView;
    @property (strong, nonatomic) NSString *tmpVideoPath;
@property (weak, nonatomic) IBOutlet UIView *trimView;
@property (strong, nonatomic) AVAssetExportSession *exportSession;
@property BOOL isSaveClicked ;
@property BOOL isWithAudio ;
@property (weak, nonatomic) IBOutlet UIImageView *musicImage;

@property (nonatomic) AVPlayerViewController *playerViewController;

    @property (nonatomic) CMTime startTime;
    @property (nonatomic) CMTime stopTime;

@property (weak, nonatomic) IBOutlet UIButton *trimBtn;

@property (weak, nonatomic) IBOutlet UIScrollView *audioTrimScrollView;

@property (nonatomic) CGFloat startTimeFL;
@property (nonatomic) CGFloat stopTimeFL;
- (IBAction)middleBarCancelButtonAction:(UIButton *)sender;
- (IBAction)middleBarPreviewAction:(UIButton *)sender;
- (IBAction)voleumSliderAction:(UISlider *)sender;
@property (weak, nonatomic) IBOutlet UISlider *voleumSlider;

@property (weak, nonatomic) IBOutlet UIView *audioTrimView;


- (IBAction)backAction:(id)sender;

- (IBAction)saveAction:(id)sender;
- (IBAction)tabCropAction:(UIButton *)sender;

@end
