//
//  CropTypeVC.h
//  Videos Sequencer
//
//  Created by iBuildx-Mac3 on 4/10/17.
//  Copyright © 2017 Saqibdb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CropTypeVC : UIViewController
- (IBAction)selectCropTypeAction:(UIButton *)sender;
- (IBAction)colorSelectionAction:(UIButton *)sender;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *colorPlates;


- (IBAction)nextAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *portraitView;
@property (weak, nonatomic) IBOutlet UIView *landscaperView;
@property (weak, nonatomic) IBOutlet UIView *selectedColorView;


@end
