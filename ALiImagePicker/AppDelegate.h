//
//  AppDelegate.h
//  ALiImagePicker
//
//
//  Created by iBuildx_Mac_Mini on 8/25/16.
//  Copyright © 2016 iBuildX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

