//
//  tempClassViewController.h
//  VidEditor
//
//  Created by ibuildx on 11/3/16.
//  Copyright © 2016 Saqibdb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface tempClassViewController : UIViewController<UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *uiView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
- (IBAction)pincAction:(UIPinchGestureRecognizer *)sender;
@property (strong, nonatomic) IBOutlet UIPinchGestureRecognizer *pinchOutlet;

@end
