//
//  EditViewController.h
//  VidEditor
//
//  Created by ibuildx on 10/28/16.
//  Copyright © 2016 Saqibdb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ALiAsset.h"
#import "SAVideoRangeSlider.h"
#import <AVKit/AVKit.h>



@interface EditViewController : UIViewController<UIScrollViewDelegate,UIGestureRecognizerDelegate,SAVideoRangeSliderDelegate , AVPlayerViewControllerDelegate , UICollectionViewDelegate , UICollectionViewDataSource>
- (IBAction)backAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *MainView;
@property (weak, nonatomic) IBOutlet UIView *shutterView;

- (IBAction)playAction:(UIButton *)sender;
- (IBAction)saveAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomOfSutter;
- (IBAction)shutterDownAction:(UIButton *)sender;
- (IBAction)cornerRadiusSlider:(UISlider *)sender;
- (IBAction)frameSizeSlider:(UISlider *)sender;
@property (nonatomic, strong) NSURL *selectedURL;
@property (weak, nonatomic) IBOutlet UISlider *cornerSlider;
@property (weak, nonatomic) IBOutlet UISlider *frameSlider;
@property (weak, nonatomic) IBOutlet UIImageView *thumOfVideo;
    @property (strong, nonatomic) NSString *tmpVideoPath;
@property (nonatomic) NSMutableArray *theScrollers;
@property BOOL isWithAudio ;
@property (strong, nonatomic) IBOutlet UIPinchGestureRecognizer *pinchObject;
@property (weak, nonatomic) IBOutlet UIView *playerView;
@property BOOL isSaveClicked ;

@property (strong, nonatomic) IBOutlet UIPinchGestureRecognizer *pinchGesture;

@property (nonatomic) AVPlayerViewController *playerViewController;

@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UIButton *cropBtn;



@property (weak, nonatomic) IBOutlet UIView *cropSampleView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cropViewWidthConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cropViewHeightConstraint;

//
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (weak, nonatomic) IBOutlet UIView *videoView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewInsideVideoView;
@property (weak, nonatomic) IBOutlet UIImageView *imageInsideScroll;
- (IBAction)coverEmptyAreaAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UICollectionView *thumbCollectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageTrailing;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageLeading;
- (IBAction)tabCropAction:(UIButton *)sender;
- (IBAction)tabTrimAction:(UIButton *)sender;


@end
