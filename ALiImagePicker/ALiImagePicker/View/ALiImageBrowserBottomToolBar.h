//
//  ALiImageBrowserBottomToolBar.h
//  ALiImagePicker
//
//  Created by ibuildx-Mac on 2016/10/18.
//  Copyright © 2016年 Saqibdb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ALiImageBrowserBottomToolBar : UIView

@property (nonatomic, strong) UIButton *fullImageBtn;

@property (nonatomic, strong) UIButton *selectedCountBtn;

@property (nonatomic, strong) UIButton *sendBtn;

@property (nonatomic, strong) UIButton *fullTitleButton;

@end
